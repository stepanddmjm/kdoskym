package kdoskym;

import java.util.Random;
import kdoskym.semtodavej.Bárt;
import kdoskym.semtodavej.Ema;
import kdoskym.semtodavej.Honza;
import kdoskym.semtodavej.Juraj;
import kdoskym.semtodavej.Martin;
import kdoskym.semtodavej.Matej;
import kdoskym.semtodavej.Michal;
import kdoskym.semtodavej.Stepan;
import kdoskym.semtodavej.Terezka;
import kdoskym.semtodavej.lukAS;

public class KdoSKym {

    private static final Random random = new Random();

    public static void main(String[] args) {

        
        
        Hrac[] hraci = new Hrac[]{
            new Bárt(),
            new Juraj(),
            new lukAS(),
            new Honza()
        };

        System.out.println(randomMoznost(hraci[random.nextInt(hraci.length)].kdo()));
        System.out.println(randomMoznost(hraci[random.nextInt(hraci.length)].sKym()));
        System.out.println(randomMoznost(hraci[random.nextInt(hraci.length)].coDelali()));
        System.out.println(randomMoznost(hraci[random.nextInt(hraci.length)].kde()));
        System.out.println(randomMoznost(hraci[random.nextInt(hraci.length)].jakDlouho()));

    }

    private static String randomMoznost(String[] moznosti) {
        return moznosti[random.nextInt(moznosti.length)];
    }

}
