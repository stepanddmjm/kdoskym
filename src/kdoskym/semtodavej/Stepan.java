package kdoskym.semtodavej;

import kdoskym.Hrac;

public class Stepan implements Hrac {

    
    
    
    @Override
    public String[] kdo() {
        return new String[] {"Bartolomej", "Terka", "Lopataaaaa", "Martin,   na GitLab"};
    }

    @Override
    public String[] sKym() {
        return new String[] {"s Matejem", "s Emcou", "se svym otcem", "se svym bratrem", "s Martinem, co si nepamatuje heslo na GitLab"};
    }
    @Override
    public String[] kde() {
        return new String[] {"na vystave psu", "pred bocnim vchodem DDMJM", "na GitLabu, na kterej si Martin nepamatuje heslo"};
    }
    @Override
    public String[] coDelali() {
        return new String[] {"hledali ovce", "jedli kocku", "pronasledovali Martina", "si pamatovali heslo na GitLab"};
    }

    @Override
    public String[] jakDlouho() {
        return new String[] {"do okolo pul druhy", "dokud si Martin nevzpomnel na heslo na GitLab"};
    }

    
    
    
}
