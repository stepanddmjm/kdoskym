
package kdoskym.semtodavej;

import kdoskym.Hrac;


public class Juraj implements Hrac{

     @Override
    public String[] kdo() {
        return new String[]{"Leonardo DiCaprio", "Rusák", "Brambory"};
    }

    @Override
    public String[] sKym() {
        return new String[]{" s pavoukem", "s Babišem", "s autem"};
    }

    @Override
    public String[] kde() {
        return new String[]{"v parku", "na chodově", "v domově důchodců"};

    }

    @Override
    public String[] coDelali() {
        return new String[]{"koukali na oblečení", "utíkali", "smáli se"};

    }

    @Override
    public String[] jakDlouho() {
        return new String[]{"ne tak dlouho", "asi rok", "sekundu"};

    }
    
    
}
