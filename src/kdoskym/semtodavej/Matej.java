package kdoskym.semtodavej;

import kdoskym.Hrac;

public class Matej implements Hrac {

    @Override
    public String[] kdo() {
        return new String[] {"Michal", "Terka", "Jezevčík", "Okresní policejní náčelník"};
    }

    @Override
    public String[] sKym() {
        return new String[] {"s Petrem", "s Matějem", "s Emčou", "s Jiřím Boumou"};
    }

    @Override
    public String[] kde() {
        return new String[] {"ve feťáckém doupěti na Opatově", "doma", "daleko", "v ananasu na dně moří"};
    }
    @Override
    public String[] coDelali() {
        return new String[] {"pracovali", "předstírali práci", "nadávali na Maxe", "žili dlouze a blaze", "otvírali lahev vína za pomoci špičaté okrasné mříže"};
    }

    @Override
    public String[] jakDlouho() {
        return new String[] {"tisíc dní a tisíc nocí", "tři vteřiny", "500 let", "leta letoucí"};
    }

    
    
    
}
