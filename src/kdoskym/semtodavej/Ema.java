package kdoskym.semtodavej;

import kdoskym.Hrac;

public class Ema implements Hrac {

    @Override
    public String[] kdo() {
        return new String[] {"Nejvlivnější čeští politici", "Pat", "Karel IV.", "Pacient psychologické léčebny", "Zápřísáhlý antifašista", "Medojed", "Karel IV., ze všech králů, kteří kdy v Čechách panovali, nejoblíbenější", "Ten, kdo to přehnal s alkoholem"};
    }

    @Override
    public String[] sKym() {
        return new String[] {"s Matem", "se zástupcem veřejného blaha", "s ptakopyskem", "s komunistou", "s počítačem", "s mikrovláknem", "s tektonickou deskou", "s něčím, co páchlo jako ponožky, vypadalo jako ponožky, ale nebyly to ponožky"};
    }

    @Override
    public String[] kde() {
        return new String[] {"ve starobylém labyrintu", "na vzdálené planetě kdesi v neznámu", "na stromě uprostřed ničeho", "v Íránu", "v Poslanecké sněmovně", "ve škole", "na cizí zahradě, kde bydlí Křemílek a Vochomůrka", "v paralelním vesmíru, kde neexistuje pivo ani počítač", "nad propastí zatracení"};
    }
    @Override
    public String[] coDelali() {
        return new String[] {"pobíhali", "létali", "kdákali", "provokovali", "snili o budoucnosti", "dělili deseticiferné číslo", "podkopávali autoritu prezidentovi korporace Sluníčko", "odmocňovali číslo 4", "poznávali mapy světa"};
    }

    @Override
    public String[] jakDlouho() {
        return new String[] {"celý rok.", "dnem i nocí.", "tři sta let.", "každý den.", "od večera do rána.", "nekonečně dlouho.", "neznámo kdy.", "napříč historií telegrafních sloupů."};
    }

    
    
    
}
