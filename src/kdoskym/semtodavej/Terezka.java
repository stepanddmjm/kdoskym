package kdoskym.semtodavej;

import kdoskym.Hrac;

public class Terezka implements Hrac {

    @Override
    public String[] kdo() {
        return new String[]{"Michael Jackson", "Terka", "knedlíky"};
    }

    @Override
    public String[] sKym() {
        return new String[]{" s brumbalem", "se Zemanem", "Emcou"};
    }

    @Override
    public String[] kde() {
        return new String[]{"v lese", "na radnici", "v tovarne na cokoladu"};

    }

    @Override
    public String[] coDelali() {
        return new String[]{"koukali na netflix", "sli", "breceli"};

    }

    @Override
    public String[] jakDlouho() {
        return new String[]{"strasne dloho", "nevime jak dlouho", "ultrakratce"};

    }

}
