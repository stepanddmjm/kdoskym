package kdoskym.semtodavej;

import kdoskym.Hrac;

public class Ben implements Hrac {

   @Override
    public String[] kdo() {
        return new String[] {"Bart", "Max"};
    }

    @Override
    public String[] sKym() {
        return new String[] {"s reditelem DDMJM"};
    }

    @Override
    public String[] kde() {
        return new String[] {"pod okny DDMJM"};
    }
    @Override
    public String[] coDelali() {
        return new String[] {"litovali dvojcata"};
    }

    @Override
    public String[] jakDlouho() {
        return new String[] {"do nedavna"};
    }

}
