package kdoskym.semtodavej;

import kdoskym.Hrac;

public class Michal implements Hrac {

    @Override
    public String[] kdo() {
       return new String[] {"Já", "Ty", "Tvoje mama","Matej","Magor od vedle","Nakdo lepsi nez ja","nekdo lepsi nez ty","Ta borka ktery se Terka boji","Decka ze skolky","Netbeany"};
    }

    @Override
    public String[] sKym() {
     return new String[] {"se mnou", "s tebou", "s tvoji mamou","s Matejem","s Magorem od vedle","s nekym lepsim nez ja","s nekym lepsim nez ty","s rozpadajicim se stolem","s obvyklou depresi"};
    }

    @Override
    public String[] kde() {
        return new String[] {"V parku", "V dome", "Na normalnim miste", "Na nenormalnim miste", "V obchodaku", "Ve vzduchu", "V ddmku", "Na programku", "V televizi", "Na hristi", "V prdeli","pod stolem","ve snu","v nastaveni"};
    }

    @Override
    public String[] coDelali() {
     return new String[] {"si hrali", "hrali si s detma", "nakupovali", "Chcali na stromy", "programovali","opravovali","sikanovali se navzajem","srali si do ust"};
    }

    @Override
    public String[] jakDlouho() {
       return new String[] {"Dlouho", "Ne tak dlouho", "docela dlouho", "Ne dost dlouho", "Chvilku", "Ne tak chvilku", "Moc dlouho", "15 minut", "15 vterin", "hodinu", "2 hodiny", "Po dobu 4 let", "Dokud nevyhraly", "Dokud nebyli hotovy", "dokud nebyli spokojeni"};
    }
    
}
