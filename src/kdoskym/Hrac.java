package kdoskym;

public interface Hrac {

    String[] kdo();

    String[] sKym();

    String[] kde();

    String[] coDelali();

    String[] jakDlouho();
}
